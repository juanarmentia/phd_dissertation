\definecolor{subtitlegrey}{rgb}{0.4,0.4,0.4}

\chapter{Introducción}{\vspace{-1cm} %{\fontfamily{fla}\LARGE \color{gray}{\selectfont  Subtítulo} }} 
\label{chap:intro}

%\epigraph{--- \ding{125}First, let me dispose of Socrates because I am sick and tired of this pretense that knowing you know nothing is a mark of wisdom.\ding{126}}{\textit{Isaac Asimov.}} 
\vspace{0.5cm}
{\fontfamily{fla}\selectfont   
%\epigraph{Un viaje de mil millas comienza
%con el primer paso..}{\textit{---\textsc{Lao-tsé} (570 a.C.- 490 a.C.)}} }
\epigraph{\textit{Cuando veo a un adulto en una bicicleta, 
no me desespero por el futuro de la humanidad}}{---\textsc{H. G. Wells.}}
  
%\vspace{1cm}
\vspace{0.5cm}
{\color{chaptergrey}{\hrule height 0.5pt}}
\vspace{0.5cm} 
    
%\PARstart
%##################################################################
\lettrine[lines=4]{E}{ste capítulo} expone una visión general de la investigación llevada a cabo en esta tesis doctoral. Se presentan las motivaciones que han impulsado esta investigación, así como las hipótesis y objetivos establecidos. Además se describe la metodología de investigación utilizada y se finaliza el capítulo explicando la estructura de esta tesis doctoral.

%##################################################################
\section{Motivación}
\label{sec:motivation}
%##################################################################
A finales del siglo XIX dio comienzo la generación industrial de electricidad, período en el que la iluminación eléctrica se extendió por calles y casas. La rápida expansión de la tecnología eléctrica la convirtió en la columna vertebral de la sociedad industrial moderna~\cite{jones1991Electrical}. Un gran número de aplicaciones vieron la luz gracias a este tipo de energía, lo que la convirtió en uno de los pilares de la Segunda Revolución Industrial~\cite{rosenberg1998ElectricityRole}. A primeros del siglo XX, aprovechando la energía eléctrica mediante baterías y líneas de corriente, se crearon los primeros electrodomésticos. El aire acondicionado en 1902, la aspiradora eléctrica en 1908 o la cafetera eléctrica en la década de 1920 son algunos ejemplos de ello. Estos novedosos objetos no eran más que un presagio del mundo del electrodoméstico en el que vivimos.\\

%Como muestra la figura \ref{fig:energyConsumptionEvolution}, el consumo de energía en Estados Unidos a lo largo de la historia ha crecido de forma notable, siguiendo la misma tendencia en el resto de países desarrollados. 

Con la aparición en los últimos años de nuevas tecnologías de información, el número de dispositivos consumidores de energía está creciendo de forma exponencial (ver figura \ref{fig:growthDevices}). Muchos de estos dispositivos son denominados inteligentes por su capacidad de conexión a Internet y por sus nuevas formas de identificación y comunicación. Como se puede observar en la figura \ref{fig:energyConsumptionEvolution}, se ha producido un aumento notable de la energía consumida en los últimos años. Otro ejemplo, entre varios en esta línea, es un informe~\cite{japan2010EnergyJapan} publicado por el gobierno de Japón, donde se publica que la energía consumida en áreas residenciales y comerciales se ha incrementado en un 30\% desde 1990. En Estados Unidos el consumo de energía en estas mismas áreas alcanzó en 2015 el 40\% de la energía total consumida en todo el país\footnote{\url{http://www.eia.gov/tools/faqs/faq.cfm?id=86\&t=1}}, al igual que ocurre a nivel mundial\footnote{\url{http://www.unep.org/sbci/AboutSBCI/Background.asp}}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.52\textwidth]{fig/chap1/connectedDevices}
	\vspace*{-0.2cm}
	\caption[Crecimiento del número de dispositivos conectados]{Crecimiento del número de dispositivos conectados$^{\ref{FootNoteForFigureCaption_growthDevices}}$.}
	\label{fig:growthDevices}
\end{figure}
\addtocounter{footnote}{1}\footnotetext{\url{http://tiny.cc/ciscointernet}\label{FootNoteForFigureCaption_growthDevices}}


\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{fig/chap1/electricityEvolution}
	\caption{Histórico del consumo de energía en EEUU (1776-2012).}
	\label{fig:energyConsumptionEvolution}
\end{figure} 

Según estudios realizados~\cite{uk2010CarbonTrust}, se ha observado que el consumo eléctrico de los equipamientos de oficina suponen más del 15\% del consumo total de una oficina. Estos mismos estudios revelan que la creciente invasión de los ordenadores, los dispositivos electrónicos, los electrodomésticos y el equipamiento de oficina es uno de los factores que influyen considerablemente en el incremento de las emisiones de dióxido de carbono (ver figura \ref{fig:outlookCO2Emissions}), lo que supone un mayor desequilibrio en la sostenibilidad del planeta en el que vivimos.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\textwidth]{fig/chap1/outlookCO2Emissions}
	\caption[Crecimiento de las emisiones de $CO_{2}$]{Crecimiento de las emisiones de $CO_{2}$$^{\ref{FootNoteForFigureCaption_outlookCO2Emissions}}$.}
	\label{fig:outlookCO2Emissions}
\end{figure}
\addtocounter{footnote}{1}\footnotetext{\url{http://www.rite.or.jp/en/}\label{FootNoteForFigureCaption_outlookCO2Emissions}}

Esta situación llevó en 2008 a la Unión Europea a regular la situación de este tipo de dispositivos, creando el reglamento 1275/2008/EC\footnote{\url{http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2008:339:0045:0052:EN:PDF}} que afecta a todos los países europeos. El objetivo de éste es resolver los requisitos de diseño ecológico aplicables al consumo de energía eléctrica de los electrodomésticos, equipos electrónicos y de oficina que no se encuentran en modo de funcionamiento “normal” (modo \textit{On}). Esta norma establece que desde 2013 todos los aparatos deben permitir al menos uno de los dos modos de espera posibles:
\begin{itemize}
\item Modo \textit{Standby}: aquél en que el equipo se halla conectado a la red eléctrica y depende de la energía procedente de dicha red para funcionar según los fines previstos y ejecuta únicamente las siguientes funciones, que se pueden prolongar por un tiempo indefinido: 
\begin{enumerate}[label=(\alph*)]
\item Función de reactivación (potencia máxima permitida de 0.5 W).
\item Visualización de información o del estado (potencia máxima permitida de 1 W).
\end{enumerate}
\item Modo \textit{Off}: aquél en que el equipo se halla conectado a la red eléctrica, si bien no está en funcionamiento o permanece totalmente inactivo (potencia máxima permitida de 1W).
\end{itemize}

Además, esta regulación decreta que cuando un equipo no esté ejecutando su actividad principal ofrecerá una función para conmutar automáticamente el equipo, tras un período breve de tiempo, al modo \textit{Off} o al modo \textit{Standby}. De aquí en adelante, cuando los dispositivos tengan activada esta función, se considerará que están funcionando en modo \textit{Auto-power Down}.\\

En la tabla \ref{ta:operationmodes} se muestra, mediante diagramas de flujo y a modo de ejemplo, el comportamiento de una cafetera en cada uno de los modos descritos.  

%\begin{figure}[H]
%	\begin{center}
%		\includegraphics[width=0.9\textwidth]{fig/chap1/OperationModes.png}
%	\end{center}
%	\caption{Comportamiento de los modos de operación existentes (Quitar mención a la cafetera y generalizar).} 
%	\label{fig:operationmodes}
%\end{figure}

\begin{table}[H]
	\centering
	%\resizebox{10.0cm}{!} {
		\begin{tabular}{|c|c|c|}
			\hline
			\cellcolor[HTML]{C0C0C0}{\scriptsize	 \textbf{Modo \textit{On-Off}}} &
			\cellcolor[HTML]{C0C0C0}{\scriptsize	 \textbf{Modo \textit{Standby}}} &
			\cellcolor[HTML]{C0C0C0}{\scriptsize	 \textbf{Modo \textit{Auto-power Down (7')}}} \\ \hline \hline
				\begin{tikzpicture}[node distance=0.8cm, baseline=5]
				
				\node (start) [startstoptable] {\tiny INICIO};
				\node (warm)  [processtable, below of=start] {\tiny Warming};
				\node (prepare)  [processtable, below of=warm] {\tiny Preparing \\ coffee \\};
				\node (finished)  [decisiontable, below of=prepare, yshift=-0.5cm, aspect=3] {\tiny ¿Ready?};
				\node (stop)  [startstoptable, below of=finished, yshift=-0.4cm] {\tiny FIN};
				
				\draw [arrow] (start) -- (warm);
				\draw [arrow] (warm) -- (prepare);
				\draw [arrow] (prepare) -- (finished);
				\draw [arrow] (finished) -- node[anchor=east] {\tiny Si}(stop);
				\draw [arrow] (finished.west) node[anchor=south] {\tiny No} |- ([xshift=-0.2cm]finished.west) |- (prepare.west);
				\end{tikzpicture} &
				\begin{tikzpicture}[node distance=0.8cm, baseline=5]
				
				\node (start) [startstoptable] {\tiny INICIO};
				\node (warm)  [processtable, below of=start] {\tiny Warming};
				\node (wait)  [processtable, below of=warm] {\tiny Waiting};
				\node (prepare)  [processtable, below of=wait] {\tiny Preparing \\ coffee \\};
				\node (finished)  [decisiontable, below of=prepare, yshift=-0.5cm, aspect=3] {\tiny ¿Ready?};
				
				\draw [arrow] (start) -- (warm);
				\draw [arrow] (warm) -- (wait);
				\draw [arrow] (wait) -- (prepare);
				\draw [arrow] (prepare) -- (finished);
				\draw [arrow] (finished.south) node[anchor=west, yshift=-0.1cm] {\tiny Sí} |- ([yshift=-0.2cm, xshift=-1.2cm]finished.south) |- (wait.west);
				\draw [arrow] (finished.west) node[anchor=south] {\tiny No} |- ([xshift=-0.2cm]finished.west) |- (prepare.west);
				\end{tikzpicture} & 
				\begin{tikzpicture}[node distance=0.8cm, baseline=5]
				
				\node (start) [startstoptable] {\tiny INICIO};
				\node (warm)  [processtable, below of=start] {\tiny Warming};
				\node (wait)  [processtable, below of=warm] {\tiny Waiting};
				
				\node (prepare)  [processtable, below of=wait, xshift=-0.9cm, yshift=-0.2cm] {\tiny Preparing \\ coffee \\};
				\node (finished)  [decisiontable, below of=prepare, yshift=-0.5cm, aspect=3] {\tiny ¿Ready?};
				\node (reset)  [processtable, below of=finished, yshift=-0.4cm] {\tiny Reset \\ timer \\};
				
				\node (timer)  [decisiontable, below of=wait, xshift=0.8cm, yshift=-1cm, aspect=3] {\tiny Timer<7'};
				\node (stop)  [startstoptable, below of=timer, yshift=-1cm] {\tiny FIN};
				
				\draw [arrow] (start) -- (warm);
				\draw [arrow] (warm) -- (wait);
				\draw [arrow] (wait.south) -- (prepare.north);
				\draw [arrow] (prepare) -- (finished);
				\draw [arrow] (wait.south) -- (timer.north);
				\draw [arrow] (finished.south) node[anchor=west, yshift=-0.1cm] {\tiny Sí} -- (reset.north);
				\draw [arrow] (reset.south) |- ([yshift=-0.2cm, xshift=-1.2cm]reset.south) |- (wait.west);
				\draw [arrow] (finished.west) node[anchor=south] {\tiny No} |- ([xshift=-0.2cm]finished.west) |- (prepare.west);
				\draw [arrow] (timer.south) node[anchor=west, yshift=-0.2cm] {\tiny No} -- (stop.north);
				\draw [arrow] (timer.east) node[anchor=south] {\tiny Sí} |- ([xshift=0.2cm]timer.east) |- (wait.east);
				\addvmargin{2mm}
				\end{tikzpicture} \\ \hline
		\end{tabular}
%	}
	\caption{Funcionamiento de una máquina de café en cada uno de los diferentes modos de operación.}
	\label{ta:operationmodes}
\end{table}

\vspace{1cm}

Pero el problema a lo largo de estos años no se ha cimentado únicamente en el ineficiente diseño de los dispositivos, sino también en el uso inadecuado que se hace de los mismos. Uno muy frecuente consiste en dejar los dispositivos encendidos consumiendo energía constantemente cuando nadie los está utilizando. En un trabajo de exploración inicial~\cite{lopez-de-armentia2012VampireAppliances} se plasmaba de forma gráfica un ejemplo de este tipo de comportamientos que tanto se repiten. Se registró el consumo energético de una cafetera ubicada en un laboratorio de investigación durante 30 días consecutivos. La figura \ref{fig:dailyEnergyConsumption} presenta una franja de tiempo de 10 días, donde se muestra claramente que los días correspondientes al fin de semana (cuarto y quinto día) la cafetera ha estado encendida sin realizar su función principal, que es la de preparar cafés.
\clearpage

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{fig/chap1/DailyEnergyConsumption}
\caption{Energía consumida por una cafetera durante 10 días consecutivos desglosada en 3 tipos de consumo.}
\label{fig:dailyEnergyConsumption}
\end{figure} 

Yang et al.~(\citeyear{yang2013Learning}) identificaron la falta de atención de los usuarios y el confort como dos de las principales causas que motivan este tipo de comportamientos. Asimismo, debido en gran parte a la naturaleza intangible de la electricidad, las personas habitualmente se mantienen al margen de comprender el consumo energético de los dispositivos. Además, tal y como señalan Schwartz et al.~(\citeyear{schwartz2010SustainablePractices}), la conciencia sobre el consumo energético de objetos de uso colectivo es insignificante en comparación con la de los dispositivos personales. Por ejemplo, no es habitual ver trabajadores por cuenta ajena preocupándose de lo que consumen los objetos que están utilizando o encontrar una mayoría de ciudadanos concienciados con un uso energéticamente eficiente de los dispositivos de uso colectivo que tienen a su disposición. ¿O acaso no es frecuente encontrar encendidas las luces de los aseos de un lugar de trabajo o una universidad?  \\

De modo que aunque se considera que la combinación de los modos de funcionamiento regulados por la Unión Europea en el reglamento 1275/2008/EC podría dar lugar a un funcionamiento energéticamente más eficiente, los usuarios de estos dispositivos, incluso los más sensibilizados con el medio ambiente, en la mayoría de los casos carecen de toda la información necesaria para hacer un uso eficiente de los mismos. 

De hecho, se puede afirmar que este comportamiento confuso es generalizado a nivel mundial. La figura \ref{fig:switchingbehavior} muestra comportamientos completamente diferentes en cuanto al apagado de las cafeteras en entornos de trabajo. Esto puede ser una evidencia de la ausencia de reglas para utilizar eficientemente las máquinas de café.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{fig/chap1/UsepatternByCountries}
		\caption[Apagado de las cafeteras en el trabajo en varios países]{Apagado de las cafeteras en el trabajo en varios países~\cite{regulation8012013}.}
		\label{fig:switchingbehavior}
	\end{center}
\end{figure}

Por lo tanto, el objetivo general de esta tesis es mejorar la eficiencia energética de los dispositivos provistos del modo \textit{Standby}, concretamente de los ubicados en espacios comunes, en los que un uso energéticamente eficiente se antoja difícil de conseguir. Algunos ejemplos de este tipo de objetos son las cafeteras, las impresoras, los proyectores o los sistemas de iluminación. Se pretende lograrlo dotando a los mismos de la capacidad de:
\clearpage
\begin{enumerate}[label=(\alph*)]
\item aprender automáticamente un modelo eficiente de funcionamiento (a través de un nuevo modo \textit{Ecoaware}) y ,
\item comunicarse, de forma que puedan compartir entre ellos sus modos de funcionamiento, posibilitando un aprendizaje más rápido --- disminución del problema de arranque en frío\footnote{Denominado en inglés \textit{cold-start problem}, es un problema potencial de los equipos basados en sistemas de información, que implican un grado de modelado de datos automatizado. En concreto, se refiere a la cuestión donde el sistema no puede extraer inferencias para los usuarios o temas sobre los que aún no ha reunido suficiente información.} --- (a través de un nuevo modo \textit{Cooperative}). 
\end{enumerate}

%Se hace necesario, por tanto, añadir a este tipo de electrodomésticos la capacidad de adaptar su funcionamiento de una forma inteligente al modo más eficiente en cada momento en cuanto a energía se refiere. Además, se cree también que si los electrodomésticos comparten datos relativos al uso que se hace de ellos con otros dispositivos del mismo tipo, la eficiencia energética de los mismos sería mayor, debido a la minimización del problema de arranque en frío. Es decir, el período de aprendizaje que los electrodomésticos necesitarían para poder operar de manera inteligente, se podría reducir si los electrodomésticos disponen inicialmente de un conjunto de datos adecuado.


%##################################################################
%El ahorro energético es uno de los grandes retos a los que se enfrenta la comunidad investigadora. Ante la imposibilidad de establecer un procedimiento para reducir el consumo eléctrico global por la diferente naturaleza de dichos consumos y ante el anteriormente citado crecimiento de elementos consumidores de energía, esta tesis doctoral se centra en reducir el uso de energía de los equipos eléctricos ubicados en áreas comunes.

%Después de analizar los consumos de diferentes dispositivos, se ha optado por una cafetera automática de cápsulas. Este electrodoméstico consume mucho y desperdician mucha energía.

%To validate our hypothesis, we have conducted a three month energy-data collection over a capsule-based coffee machine which is placed in our laboratory. This monitoring is performed to analyze how laboratory-workers use the device under-study and to devise solutions to reduce unnecessarily wasted energy (i.e. people awareness and technical strategies) when device misusing occurs\footnote{For this article,the term 'misuse' is applied when is more efficient to perform an operating mode than another for energy reduction proposes}. In a first analysis of the collected data, we observed a random use of the coffee-maker, thus no rationale was found in terms of the device usage: \textit{1)} different number of coffees per day, \textit{2)} these coffees were prepared at different hours during the working-day, \textit{3)} people applied different usages - operating modes -, when preparing a hot-drink, i.e. some left the coffee-maker on \textit{Standby}, and others left the device switched-off (\textit{No-power} mode) after its utilization . Such randomness is translated into a non negligible waste of energy since, ideally, the device operation should be adapted to the expected number of coffees to be prepared. Thus, depending on the hour of the day and the frequency of coffee intakes, the coffee maker must remain in \textit{Standby} mode - high number of coffees -, or in contrast, to be switched off - low activity.\\
%##################################################################


%##################################################################
\section{Hipótesis y objetivos}	
\label{sec:hypothesis}
%##################################################################
%Acorde a lo ya expuesto, se desarrolla la hipótesis principal de la investigación, que se expresa en los siguientes términos:
%
%\begin{quote}
%	\textit{$\mathbf{H_{A}}$: Una colaboración entre dispositivos eléctricos basada en la compartición de sus patrones de uso, se traduce en una reducción significativa de su consumo energético.}
%\end{quote}
%
%Como ya se ha explicado con anterioridad, la hipótesis principal se basa en la mayor eficiencia energética de los dispositivos que tienen la capacidad de adaptar dinámicamente su modo de funcionamiento a su patrón de uso. Por lo tanto, se debe dar la veracidad de una segunda hipótesis:
%
%\begin{quote}
%	\textit{$\mathbf{H_{B}}$: Una conmutación automática e inteligente del modo de funcionamiento de los dispositivos eléctricos, se traduce en una reducción significativa de su consumo energético.}
%\end{quote}

Acorde a lo ya expuesto, se desarrollan las hipótesis de la investigación. La primera de ellas se expresa en los siguientes términos:

\begin{quote}
	\textit{$\mathbf{H_{A}}$: Una conmutación automática e inteligente del modo de funcionamiento de los dispositivos eléctricos, se traduce en una reducción significativa\footnote{La directiva relativa a la eficiencia energética (2012/27/UE) establece entre otros criterios la elaboración de medidas que garanticen un ahorro anual de energía del 1.5\% para los usuarios finales hasta 2020. Por lo tanto, se considera en esta tesis doctoral que una reducción significativa del consumo energético es un ahorro del 1.5\% de la energía consumida.} de su consumo energético.}
\end{quote}

Considerando veraz la hipótesis $ H_{A} $, se presenta la siguiente hipótesis:

\begin{quote}
\textit{$\mathbf{H_{B}}$: Una colaboración entre dispositivos eléctricos basada en la compartición de sus patrones de uso, se traduce en una aceleración del proceso de ahorro energético, lo que conlleva una reducción significativa de su consumo energético durante sus primeras semanas de uso.}
\end{quote}
\clearpage

Como ya se ha explicado con anterioridad, la hipótesis $ H_{B} $ se basa en la mayor eficiencia energética de los dispositivos que tienen la capacidad de adaptar dinámicamente su modo de funcionamiento a su patrón de uso. Por lo tanto, si y sólo si se demuestra que la hipótesis $ H_{A} $ es verdadera, se podrá cumplir la hipótesis $ H_{B} $.

Para conformar la visión planteada en la investigación y teniendo en cuenta las hipótesis de partida, se ha definido una serie de objetivos, tanto para la creación del modo de funcionamiento \textit{Ecoaware}, como del modo \textit{Cooperative}. Se enumeran primero los objetivos establecidos para crear satisfactoriamente el nuevo modo de funcionamiento \textit{Ecoaware}, ya que su consecución es imprescindible para poder validar la hipótesis $ H_{B} $:

\begin{itemize}
\item \textbf{Objetivo A.1:} definición del proceso que se va a llevar a cabo para analizar el funcionamiento de un electrodoméstico a nivel energético. Se debe medir el consumo de energía en el período de tiempo inmediato a alimentar con corriente el dispositivo, cuando se encuentra funcionando en modo \textit{Standby} y cuando lo está haciendo en modo \textit{On} desarrollando la función principal para la que ha sido diseñado.
\item \textbf{Objetivo A.2:} diseño y desarrollo de un sistema, hardware y software, que debe medir y almacenar todas las acciones de consumo energético de un dispositivo. Este objetivo conlleva la definición del modelo de datos que permite describir cuantitativamente y cualitativamente la energía consumida por los dispositivos a lo largo del tiempo. A su vez, se ha de determinar el sistema de almacenamiento a utilizar.
\item \textbf{Objetivo A.3:} definición y aplicación del método de aprendizaje automático que permite obtener una mayor precisión en la predicción de uso del dispositivo. Es necesario analizar diferentes métodos para discernir cuál es el que mejor se ajusta a la problemática.
\item \textbf{Objetivo A.4:} validación de los módulos hardware y software desarrollados para comprobar que su funcionamiento se adecua a lo que se ha propuesto.
\item \textbf{Objetivo A.5:} evaluación de la solución integral que propone cambiar el modo de funcionamiento de los dispositivos en función de su patrón de uso. Se mide cuantitativamente el ahorro energético de los dispositivos funcionando en modo \textit{Ecoaware} y se compara respecto a los modos de funcionamiento existentes (\textit{On-Off, Standby, Auto-power Down}).
\end{itemize}

A continuación, se exponen los objetivos definidos para crear el modo de funcionamiento \textit{Cooperative} y evaluar su impacto:

\begin{itemize}
\item \textbf{Objetivo B.1:} definición de la arquitectura que debe englobar los dispositivos y los flujos de intercambio de datos entre los mismos.
\item \textbf{Objetivo B.2:} determinación del algoritmo de similitud entre dispositivos. Para ello es necesario realizar un estudio del estado del arte de los algoritmos ya existentes y evaluar si alguno de ellos se ajusta a la problemática o es necesario definir uno nuevo que se adecue a la misma. Una parte esencial es el modelado del perfil de los dispositivos, ya que el grado de similitud entre dos objetos depende directamente de su perfil.
\item \textbf{Objetivo B.3:} desarrollo del algoritmo de similitud definido anteriormente y su validación para evaluar su funcionamiento de acuerdo a los requisitos que se establezcan.
\item \textbf{Objetivo B.4:} definir el modelo de comunicación entre los dispositivos. El primer paso debe ser definir qué información se quiere intercambiar y qué flujo de información se va a producir. Una vez realizada esta tarea, se ha de valorar tanto la idoneidad de los diferentes \textit{middleware} existentes para atender las necesidades que se plantean, como la posibilidad de crear una solución adaptada.
\item \textbf{Objetivo B.5:} desarrollo de la infraestructura de comunicación determinada anteriormente y su validación para determinar que responde a los requisitos de la solución demandada.
\item \textbf{Objetivo B.6:} evaluación de la solución colaborativa expuesta en esta tesis doctoral para comprobar las ventajas y limitaciones de la propuesta planteada. Se mide cuantitativamente el ahorro energético de los dispositivos funcionando en modo \textit{Cooperative} y se compara respecto al modo de funcionamiento \textit{Ecoaware}.
\end{itemize}


%##################################################################
\section{Metodología de investigación}
\label{sec:methodology}
%##################################################################
En busca de alcanzar los objetivos plasmados en la sección \ref{sec:hypothesis} se ha definido una estrategia de investigación (ver figura \ref{fig:methodology}). Ésta engloba las siguientes actividades:

\begin{enumerate}
\item \textbf{Revisión bibliográfica: }llevar a cabo una revisión sistemática de los estudios e investigaciones más relevantes, atendiendo a diversos marcos de revisión, relacionados con las áreas abordadas en la tesis doctoral.
\item \textbf{Definición de hipótesis y objetivos: }la revisión bibliográfica ha dado lugar a una línea de investigación abierta relacionada con la posibilidad de reducir el consumo energético de los dispositivos eléctricos capaces de funcionar en diferentes modos (\textit{On/Off/Standby}). Por lo tanto, en esta tarea se definen las hipótesis de partida y se identifican los objetivos, tanto generales como específicos, perseguidos durante el desarrollo de la tesis doctoral. 
\item \textbf{Diseño y desarrollo de la propuesta: }esta tarea consiste en el diseño y el desarrollo de la arquitectura propuesta, así como la de todos los elementos que la componen. 
\item \textbf{Experimentación y evaluación: }evaluar las soluciones propuestas, tanto para los dispositivos que individualmente aprenden a funcionar de forma dinámica como para la solución global en la que los dispositivos se comunican entre sí. La experimentación ha permitido comprobar las aportaciones, ventajas y limitaciones de las propuestas realizadas en la investigación.
\item \textbf{Validación: }Difusión en la comunidad científica de los resultados obtenidos para la consideración y aprobación de la tesis.
\end{enumerate}

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{fig/chap1/metodologiaInvestigacion}
\caption{Metodología de investigación utilizada.}
\label{fig:methodology}
\end{figure} 

%##################################################################
\section{Organización de la tesis}
\label{sec:organisation}
%##################################################################
Esta tesis doctoral está estructurada en seis capítulos, incluyendo el actual de introducción.

El capítulo \ref{chap:soa} contiene una revisión bibliográfica de diferentes áreas relacionadas con esta tesis doctoral. Se revisan los diferentes dispositivos existentes diseñados para el ahorro de energía; las principales plataformas hardware en el ámbito de Internet de las Cosas; arquitecturas para Internet de las Cosas y redes sociales dentro del mismo contexto; algoritmos de similitud entre perfiles; y métodos de aprendizaje automático y predicción.\\

El capítulo \ref{chap:proposal} introduce de una manera formal cuál es el problema a resolver. Se presenta también la solución que se propone para hacer frente a la problemática descrita, describiendo la arquitectura basada en redes sociales que se ha adoptado.\\

En el capítulo \ref{chap:implementation} se describe cómo se ha implementado la solución adoptada. Se detalla el diseño y desarrollo de todos los elementos de la arquitectura, desde los propios dispositivos hasta la infraestructura que las comunica, pasando por los adaptadores hardware que se han incorporado a las cafeteras que forman parte del experimento, la plataforma web para registrar dispositivos y los módulos de cálculo, tanto de similitud entre los mismos, como de predicción de uso para aumentar su eficiencia energética.\\

El capítulo \ref{chap:evaluation} contiene la evaluación y los resultados de la solución que se ha propuesto, permitiendo así conocer cuál es el alcance de las aportaciones que se han identificado. \\

Por último, el capítulo \ref{chap:conclusions} recoge las conclusiones extraídas de la investigación realizada. Además, se enumeran las contribuciones de esta tesis doctoral y las publicaciones que la respaldan. Para finalizar, se presentan las posibles líneas futuras de investigación que permitan avanzar sobre el trabajo realizado y aquí expuesto.


