package com.testsketcher2android;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
//combo box
import android.widget.Spinner;
//Button
import android.widget.Button;
//ListBox y ComboBox
import android.widget.ArrayAdapter;


import android.content.Intent;
import android.view.View;	
 
public class EditWindow extends Activity {         
        
@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);        
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);        
    setContentView(R.layout.editwindow);  
    init();       
    }
        
public void init() {
	Button boton_EditWindow_saveButton = (Button) findViewById(R.id.EditWindow_saveButton);
        boton_EditWindow_saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), ViewWindow.class);
                startActivityForResult(myIntent, 0);
            }

    });
	Button boton_EditWindow_cancelButton = (Button) findViewById(R.id.EditWindow_cancelButton);
        boton_EditWindow_cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), ViewWindow.class);
                startActivityForResult(myIntent, 0);
            }

    });
	Spinner spinner = (Spinner) findViewById(R.id.EditWindow_provinciaComboBox);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
          this, R.array.EditWindow_provinciaComboBox_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

    }
}
  